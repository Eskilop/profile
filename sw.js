var CACHE_NAME = 'bio-cache-v1';
var urlsToCache = [
	'/bio/manifest.webmanifest',
	'/bio/main.html',
	'/bio/index.html',
	'/bio/gpg.html',
  '/bio/info.html',
	'/bio/js/router.js',
	'/bio/js/utils.js',
	'/bio/media/gpg_qr.svg',
	'/bio/media/icons/element.png',
	'/bio/media/icons/xmpp.png',
	'/bio/media/icons/jami.png',
	'/bio/media/icons/pixelfed.png',
	'/bio/media/icons/gitea.png',
	'/bio/media/icons/protonmail.png',
];


self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
          .then(function(cache) {
            return cache.addAll(urlsToCache);
          })
      );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request)
        .then(function(response) {
          // Cache hit - return response
          if (response) {
            return response;
          }

          return fetch(event.request).then(
            function(response) {
              // Check if we received a valid response
              if(!response || response.status !== 200 || response.type !== 'basic') {
                return response;
              }
  
              // IMPORTANT: Clone the response. A response is a stream
              // and because we want the browser to consume the response
              // as well as the cache consuming the response, we need
              // to clone it so we have two streams.
              var responseToCache = response.clone();
  
              caches.open(CACHE_NAME)
                .then(function(cache) {
                  cache.put(event.request, responseToCache);
                });
  
              return response;
            }
          );
        })
      );
  });
