var dRoot = window.location.hostname;
var base = "/bio/#/";
var protocol = "https:";

function init() {
    window.onhashchange=route;
    if (window.location.hash == "" || !window.location.hash.startsWith("#/") || (window.location.hash.split('#').length - 1) > 1) {
        setPath("main");
    }
    route();
}

function route() {
    path = getPath();

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) { document.getElementById("content").innerHTML = this.responseText; }
            if (this.status == 404) { document.getElementById("content").innerHTML = "Can't find requested document: " + document.location.href; }
        }
    }
    if (typeof path == undefined) { path = "main"; }
    var requestUrl = protocol + "//" + dRoot + base.replace("#/", "") + path + ".html";
    xhr.open("GET", requestUrl, true);
    xhr.send();
}

function getPath() {
    var path = window.location.hash.replace("#/", "");
    if (window.location.hash == "" || !window.location.hash.startsWith("#/") || (window.location.hash.split('#').length - 1) > 1) {
        path = "main";
        setPath("main");
    }
    return path !== "" ? path : "main";
}

function setPath(path) {
    window.location.href = protocol + "//" + dRoot + base + path;
    window.location.reload();
}

function getYears() {
    document.getElementById("years").innerHtml = (new Date()).getFullYear() - 1998;
}
